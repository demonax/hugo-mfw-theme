---
title: "{{ replace .Name "-" " " | title }}"
description:  
date: {{ .Date }}
draft: true
---

Person, Problem, Pain

Amplify, Aspire

Story, Struggle, Solution

Testimony, Transformation

Objections, Offer

Reassure, Response